from django.shortcuts import render
import json
import time
from os import path
import serial
import csv
from datetime import datetime
from django.http import HttpResponseForbidden, HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from communication.nv9biller import Biller
from communication.nv9biller import ssp

tx_id = 100001

# Create your views here.
@csrf_exempt
def charge(request):
    global tx_id
    if request.method == 'POST':
        if 'application/json' in request.META['CONTENT_TYPE']:
            print(request.body)
            data = json.loads(request.body)
            charge_amount = data.get("charge_amount", None)
            if charge_amount is not None:
                if not isinstance(charge_amount, int):
                    return JsonResponse({"status_code": 400,
                                         "text": "Charge Amount must be an integer"})
                elif charge_amount % 5 or charge_amount <= 0:
                    return JsonResponse({"status_code": 400,
                                         "text": "Charge Amount must be divisible by 5 and positive"})
                else:
                    try:

                        file_exists = path.exists("Transaction Details.csv")
                        with open('Transaction Details.csv', "a") as f:
                            f_write = csv.writer(f, dialect='excel')
                            if not file_exists:
                                f_write.writerow(['Transaction Id', 'Required Amount', 'Paid amount', 'Date & Time'])

                        biller = Biller("/dev/ttyACM0")
                        print('-------------------')
                        print('Biller test program')
                        print('SN: {:08X}'.format(biller.serial))
                        print('--------------------')
                        print('Enabling biller...')
                        biller.channels_set(biller.CH_ALL)
                        biller.display_enable()
                        biller.enable()

                        events = biller.poll()
                        for event in events:
                            # print(event)
                            print(event._code)
                            if event._channel is not None:
                                print(event._channel)
                            time.sleep(0.5)

                        paid_amount = 0

                        while charge_amount - paid_amount > 0:
                            print("Please insert a note within 10 seconds")

                            while True:
                                try:
                                    t_end = time.time() + 10
                                    a = float(0)
                                    in_amount = paid_amount
                                    rejected = False
                                    while time.time() < t_end:
                                        events = biller.poll()
                                        for event in events:
                                            # print(event)
                                            print(event._code)
                                            if event._channel is not None:
                                                print(event._channel)
                                                a = str(event._channel)
                                                a = float(a)
                                                in_amount = in_amount + a
                                                if in_amount > charge_amount:
                                                    print('got here')
                                                    rejected = True
                                                    biller._transmit(ssp.CMD_REJECT)
                                                in_amount = in_amount - a
                                            time.sleep(0.5)

                                    # if 'Rejected' in events:
                                    #   print("Transaction rejected.Press Ctrl+C to initialize a new transaction")
                                    # else:
                                    paid_amount = paid_amount + a
                                    if rejected:
                                        paid_amount = paid_amount - a
                                    elif a > 0:
                                        print("Received {} Taka".format(a))
                                        break
                                    elif a == 0:
                                        print("Timeout")
                                        print("Total paid {} Taka".format(paid_amount))
                                        print('Disabling biller...')
                                        biller.disable()
                                        biller.display_disable()
                                        biller.channels_set(None)

                                        now = datetime.now()
                                        date_time = now.strftime("%d/%m/%Y %H:%M:%S")

                                        c_row = [tx_id, charge_amount, paid_amount, date_time]
                                        with open('Transaction Details.csv', "a") as f:
                                            f_write = csv.writer(f, dialect='excel')
                                            f_write.writerow(c_row)

                                        tx_id += 1
                                        return JsonResponse({"status_code": 400,
                                                             "text": "timeout"})
                                    else:
                                        print("Your entered amount is incorrect. Please, Call 018888 to get your money")
                                        break
                                except KeyboardInterrupt:
                                    break

                        print("Total paid {} Taka".format(paid_amount))
                        print('Disabling biller...')
                        biller.disable()
                        biller.display_disable()
                        biller.channels_set(None)

                        now = datetime.now()
                        date_time = now.strftime("%d/%m/%Y %H:%M:%S")

                        c_row = [tx_id, charge_amount, paid_amount, date_time]
                        with open('Transaction Details.csv', "a") as f:
                            f_write = csv.writer(f, dialect='excel')
                            f_write.writerow(c_row)

                        tx_id += 1
                        resp = {"charge_amount": charge_amount, "received_amount": paid_amount, "tx_id": tx_id}
                        if paid_amount == charge_amount:
                            resp["status_code"] = 200
                            resp["received_successfully"] = True
                        else:
                            resp["status_code"] = 400
                            resp["received_successfully"] = False
                        return JsonResponse(resp)
                    except serial.serialutil.SerialException:
                        return JsonResponse({"status_code": 400,
                                             "text": "No device found"})

            else:
                return JsonResponse({"status_code": 400,
                                     "text": "charge_amount not specified"})
        else:
            return JsonResponse({"status_code": 400,
                                 "text": "Only JSON data accepted"})
    return JsonResponse({"status_code": 400,
                         "text": "Only POST data accepted"})
