from django.urls import path, include
from . import views

urlpatterns = [
    path('charge/', views.charge, name='charge'),
]