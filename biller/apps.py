from django.apps import AppConfig


class BillerConfig(AppConfig):
    name = 'biller'
